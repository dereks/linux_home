#!/usr/bin/env python3
'''
Compare Output of lqtbatch

For use with comparing lqtbatch output and,
print out the only differences.

Usage:
    $ ./comp-out.py output1 output2 > diff.out
    $ less diff.out

Legends (color):
    YELLOW - content same but status different
    RED - content different

Dependencies:
    prettytable
'''


import sys
from prettytable import PrettyTable
import os


PIVOT_ID=['InputID', 'LQTUID']

LIGHTRED='\x1b[1;31m'
LIGHTYELLOW='\x1b[1;33m'
WHITE='\x1b[1;37m'
NORMAL='\x1b[00m'

COLOR_ENABLE=True
MATCH_RULE=True


def colorize(color, text):
    if COLOR_ENABLE:
        return color + text + NORMAL
    return text


def main(argc, argv):
    if argc < 3:
        print('usage: comp-out file1 file2 [seperator]')
        return 0

    seps = '\t'
    if argc > 3:
        for i in range(3, argc):
            #print >>sys.stderr, '%d = %s' % (i, argv[i])
            if argv[i][0] == '-':
                if argv[i] == '-C':
                    global COLOR_ENABLE
                    print('Color disabled.', file=sys.stderr)
                    COLOR_ENABLE = False

                if argv[i] == '-M':
                    global MATCH_RULE
                    print('Ignore Match Rule Label changes.', file=sys.stderr)
                    MATCH_RULE = False

            else:
                print('Using [%s] as seperator.' % argv[i], file=sys.stderr)
                seps = argv[i]


    rawline1 = (x.strip('\n') for x in open(argv[1]))
    line1 = (x for x in rawline1 if x)
    firstline1 = next(line1)
    topline1 = firstline1.split(seps)

    rawline2 = (x.strip('\n') for x in open(argv[2]))
    line2 = (x for x in rawline2 if 2)
    firstline2 = next(line2)
    topline2 = firstline2.split(seps)



    # get common pivot ID
    if topline1 != topline2:
        print("Incompatible text dumps to compare.")
        return 1
    pivot_index = -1
    for pivot in PIVOT_ID:
        try:
            pivot_index = topline1.index(pivot)
            print('Field [%s] chosen as pivot.' % pivot, 'Index=%d' % pivot_index, file=sys.stderr)
            break
        except ValueError:
            pass
    if pivot_index == -1:
        print('Error finding pivot.', file=sys.stderr)
        return 1

    address_index = topline1.index('Address')
    if (address_index == -1):
        print('Error finding Address field.', file=sys.stderr)
        return 1

    match_rule_label_index = -1
    try:
        match_rule_label_index = topline1.index('MatchRuleLabel')
    except ValueError:
        pass
    if (match_rule_label_index == -1):
        print('Match Rule Label not found.', file=sys.stderr)
    else:
        print('Match Rule Label found at ', match_rule_label_index, file=sys.stderr)

    # make status map
    status_map = {}
    for i in range(len(topline1)):
        if i == pivot_index:
            continue
        hdr = topline1[i]
        if hdr.endswith('Status'):
            continue
        try:
            status_index = topline1.index(hdr + 'Status')
            status_map[i] = status_index
        except ValueError:
            continue

    # read 1st data
    dict1 = { }
    for r in line1:
        arr = r.split(seps)
        dict1[arr[pivot_index]] = (arr, r)  #arr

    # read 2nd data and stores only the diff data
    diff_list = []
    lineno = 0
    for r in line2:
        lineno += 1
        arr2 = r.split(seps)
        arr1, s = dict1[arr2[pivot_index]]
        #arr1 = s.split(seps)
        #arr1 = dict1[arr2[pivot_index]]
        # if arr1 == arr2:
        if arr1[address_index] != arr2[address_index] or (MATCH_RULE and match_rule_label_index != -1 and arr1[match_rule_label_index] != arr2[match_rule_label_index]):
            diff_list.append((arr1, arr2, lineno, s, r))
        else:
            del dict1[arr2[pivot_index]]

        #if ((arr1[address_index] == arr2[address_index] and match_rule_label_index != -1 and arr1[match_rule_label_index] == arr2[match_rule_label_index])
        #        or (arr1[address_index] == arr2[address_index])):
        #    del dict1[arr2[pivot_index]]
        #    continue
        #diff_list.append((arr1, arr2, lineno, s, r))

    # print out table
    for rno, res in enumerate(diff_list):
        arr1 = res[0]
        arr2 = res[1]
        lineno = res[2]
        oline1 = res[3]
        oline2 = res[4]
        record_info = colorize(WHITE, "%d of %d" % (rno+1, len(diff_list)))
        t = PrettyTable([record_info, os.path.basename(argv[1]), 'S1', os.path.basename(argv[2]), 'S2'])
        t.add_row([topline1[pivot_index], arr1[pivot_index], '', arr2[pivot_index], ''])
        for i in range(len(arr2)):
            try:
                hdr = topline1[i]
            except IndexError:
                print("i=", i, file=sys.stderr)
                print("len(topline1)=", len(topline1), "  topline1=", repr(firstline1), file=sys.stderr)
                print("len(arr1)=", len(arr1), "  line1=", repr(oline1), file=sys.stderr)
                print("len(arr2)=", len(arr2), "  line2=", repr(oline2), file=sys.stderr)
                raise
            if hdr.endswith('Status'):
                continue;
            if i != pivot_index:
                s1 = '' if i not in status_map else arr1[status_map[i]]
                s2 = '' if i not in status_map else arr2[status_map[i]]
                color = NORMAL
                if arr1[i] != arr2[i]:
                    color = LIGHTRED
                elif s1 != s2:
                    color = LIGHTYELLOW
                s = ('' if color == NORMAL else ' * ')
                t.add_row([
                    colorize(color, s + topline1[i] + s),
                    colorize(color, arr1[i]),
                    colorize(color, s1),
                    colorize(color, arr2[i]),
                    colorize(color, s2)
                    ])
        print(t)


if __name__=='__main__':
    exit(main(len(sys.argv), sys.argv))

