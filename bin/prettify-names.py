#!/usr/bin/env python3

import difflib
import sys
import os


def longest_common_substring(a, b):
    sm = difflib.SequenceMatcher(None, a, b);
    r = sm.find_longest_match(0, len(a), 0, len(b))
    if r == (0, 0, 0):
        return None
    return a[ r.a : (r.a + r.size) ]


def get_common_substring(strlist):
    ''' find the common substring of all substring '''
    if len(strlist) == 1:
        return strlist[0]
    comstr = None
    for n, s in enumerate(strlist):
        if n == 0:
            comstr = s
        else:
            comstr = longest_common_substring(comstr, s)
    return comstr


def get_rename_pair(fname, commonstr, ignorelist=None):
    newname = fname
    if len(commonstr) > 0:
        if ignorelist is None or (ignorelist is not None and commonstr not in ignorelist):
            newname = fname.replace(commonstr, ' ')
    return (fname, newname.replace('\u3000', '').replace('  ', ' '))


def rename_filename_pair(dirname, thepair):
    if thepair[0] == thepair[1]: return False
    oldname = os.path.join(dirname, thepair[0])
    newname = os.path.join(dirname, thepair[1])
    try:
        os.rename(oldname, newname)
        return True
    except (IOError, OSError) as ex:
        print (ex, oldname, newname)
        return False


def process_folder(pathname):
    thedir = pathname
    # try expands all env vars
    while thedir.find('~') != -1 or thedir.find('$') != -1:
        thedir = os.path.expandvars(thedir)
        thedir = os.path.expanduser(thedir)

    for dirpath, dirnames, filenames in os.walk(thedir):
        if not filenames: continue
        comstr = get_common_substring(filenames)
        rename_pairs = [get_rename_pair(x, comstr, ('.mp3',) ) for x in filenames]
        for pair in rename_pairs:
            rename_filename_pair(dirpath, pair)


def main(argc, argv):
    for x in argv[1:]:
        print ('Processing ', x, '...', end=' ')
        process_folder(x)
        print ('done.')


if __name__ == '__main__':
    exit(main(len(sys.argv), sys.argv))

