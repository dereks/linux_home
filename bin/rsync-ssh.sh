#!/usr/bin/env sh
set -x
rsync -avzz -e "ssh" --progress "$@"
