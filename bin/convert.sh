#!/usr/bin/env sh

vidconv=$(getconverter.sh)
if [ -z "$vidconv" ]
then
    echo "Unable to find ffmepg or avconv."
    exit 1
fi

if [ -z "$1" ]
then
    echo "usage: convert.sh inputfile [resolution]"
    echo 'where:'
    echo '  resolution = "640x320"'
    echo
    exit 1
fi


filename="$1"
shift
extension="${filename##*.}"
fname="${filename%.*}"

output="${fname}.mp4"
if [ "$output" = "$filename" ]
then
    thebase="${fname}"
    while [ -e "${output}" ]
    do
        thebase="${thebase}a"
        output="${thebase}.mp4"
    done
fi


# try using libfdk_aac
audiolib=$(ffmpeg -encoders 2>&1 | grep 'libfdk_aac' | awk '{ print $2; }')
#vbr1 = 20~32 kbps/channel
#vbr2 = 32~40 kbps/channel
#vbr3 = 48~46 kbps/channel
#vbr4 = 64~72 kbps/channel
#vbr5 = 96~112 kbps/channel

#temporary for high quality
#vbr_option='-vbr 2'
vbr_option='-vbr 5'
audiofreq=''
bitrate=''


# using default AAC
if [ -z "$audiolib" ]
then
    audiolib=$(ffmpeg	-encoders 2>&1 | grep 'Advanced Audio Coding' | awk '{ print $2; }')
    # -q:a limit 0.1~2
    vbr_option='-q:a 0.23'
    #vbr_option='-q:a 2'
    #vbr_option=''
    audiofreq=''
    #bitrate='-b:a 128k'
fi


# using VORBIS
if [ -z "$audiolib" ]
then
    audiolib=libvorbis
    vbr_option='-aq 4'
    audiofreq='-ar 44100'
    #audiofreq='-ar 22050'
    #bitrate='-b:a 64k'
    bitrate='-b:a 128k'

fi


until [ -z "$1" ];
do
    size=$(echo $1 | grep -E '[[:digit:]]{3,}x[[:digit:]]{3,}')
    if [ -z "$size" ]
    then
        crf_val=$1
    else
	shift
	crf_val=$1
    fi
    shift
done


if [ -z "$crf_val" ]
then
    crf_val=23
fi

THREADS=$(echo $(nproc)*4/3 | bc)
#THREADS=$(echo $(nproc)+1 | bc)

echo 'Will be using:' $vidconv
echo 'Audio:' $audiolib ${audiofreq} ${vbr_option} ${bitrate}
#echo 'Threads:' $(nproc)
echo 'Threads:' ${THREADS}
echo


echo 'cwd:    ' $(pwd)
echo 'input:  ' ${filename}
echo 'output: ' ${output}
echo 'size:   ' ${size}
echo 'crf:    ' ${crf_val}
if [ -z "$size" ]
then
    echo "using default size"
    echo
    nice -n 15 ionice -c3 $vidconv -i "$filename" -vcodec libx264 -crf ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads ${THREADS} "${output}"
else
    echo "use specified size:" $size
    echo
    nice -n 15 ionice -c3 $vidconv -i "$filename" -s "$size" -vcodec libx264 -crf ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads ${THREADS} "${output}"
fi

