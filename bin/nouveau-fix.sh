#!/usr/bin/env sh

# ** write this to /etc/X11/xorg.conf.d/10-gpu.conf
#Section "ServerLayout"
#        Identifier "layout"
#        Screen 0 "nouveau"
#        Inactive "intel"
#EndSection
#
#Section "Device"
#        Identifier  "nouveau"
#        Driver      "nouveau"
#        BusID       "PCI:1:0:0" # Sample: "PCI:1:0:0"
#EndSection
#
#Section "Screen"
#        Identifier "nouveau"
#        Device "nouveau"
#EndSection
#
#Section "Device"
#        Identifier  "intel"
#        Driver      "intel"
#        BusID       "PCI:0:2:0"  # Sample: "PCI:0:2:0"
#EndSection
#
#Section "Screen"
#        Identifier "intel"
#        Device "intel"
#EndSection

############################################3
#
#Screen 0: minimum 320 x 200, current 2560 x 1440, maximum 16384 x 16384
#HDMI-2 connected 2560x1440+0+0 (normal left inverted right x axis y axis) 527mm x 296mm
#   2560x1440     59.95*+
#   2048x1152     59.90
#   1920x1200     59.95
#   1920x1080     60.00    50.00    59.94    24.00    23.98
#   1920x1080i    60.00    50.00    59.94
#   1600x1200     60.00
#   1680x1050     59.88
#   1280x1024     75.02    60.02
#   1280x800      59.91
#   1152x864      75.00
#   1280x720      60.00    50.00    59.94
#   1024x768      75.03    60.00
#   800x600       75.00    60.32
#   720x576       50.00
#   720x480       60.00    59.94
#   640x480       75.00    60.00    59.94
#   720x400       70.08
#eDP1 connected primary (normal left inverted right x axis y axis)
#   1920x1080     60.02 +  40.03
#HDMI1 disconnected (normal left inverted right x axis y axis)
#VIRTUAL1 disconnected (normal left inverted right x axis y axis)
#
# from the sample above use the following to setup the monitor output
xrandr --setprovideroutputsource Intel nouveau
xrandr --output eDP1 --mode 1920x1080 --output HDMI-2 --mode 2560x1440 --right-of eDP1
