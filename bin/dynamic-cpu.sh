#!/usr/bin/env sh

# get cpufreq utility name
cputool=$( (command -v cpufreq-set > /dev/null 2>&1 && echo "cpufreq") || (command -v cpupower > /dev/null 2>&1 && echo "cpupower") )
if [ "${cputool}" = "cpufreq" ]
then
	CPUFREQ_INFO="cpufreq-info"
else
	CPUFREQ_INFO="cpupower frequency-info"
fi


LOW_FREQ=$( ${CPUFREQ_INFO} | grep 'hardware limits' | head -n 1 | awk '{ print $3$4;}' )
HI_NUMB=$( ${CPUFREQ_INFO} | grep 'hardware limits' | head -n 1 | awk '{ print $6;}' )
HI_UNIT=$( ${CPUFREQ_INFO} | grep 'hardware limits' | head -n 1 | awk '{ print $7;}' )

# halves the frequency if on batterycpi
acpi | grep -i discharging | grep -i remaining > /dev/null
USE_BATTERY=$?
if [ $USE_BATTERY -eq 0 ]
then
    echo Using half-speed on battery.
    HI_FREQ=$(echo "scale=2; $HI_NUMB / 2" | bc )${HI_UNIT}
else
    echo Using full speed on AC.
    HI_FREQ=${HI_NUMB}${HI_UNIT}
fi

echo Low freq:  ${LOW_FREQ}
echo High freq: ${HI_FREQ}

for i in $(seq 0 $(echo $(nproc)-1 | bc)); do
    if [ "${cputool}" = "cpufreq" ]
    then
        sudo cpufreq-set -d $LOW_FREQ -u $HI_FREQ -c $i
        sudo cpufreq-set -g powersave -c $i
    else
        sudo cpupower -c $i frequency-set -d $LOW_FREQ -u $HI_FREQ
        sudo cpupower -c $i frequency-set -g powersave
    fi
done

if [ "${cputool}" = "cpufreq" ]
then
    ${CPUFREQ_INFO} | grep -E '(current policy|CPU frequency|governor)'
else
    sudo cpupower -c "$(seq -s ',' 0 $(echo $(nproc)-1 | bc))" frequency-info | grep -E '(current policy|CPU frequency|governor)'
fi
