#!/usr/bin/env sh

vidconv=$(getconverter.sh)
if [ -z "$vidconv" ]
then
    echo "Unable to find ffmepg or avconv."
    exit 1
fi

if [ -z "$1" ]
then
    echo "usage: convert.sh inputfile [resolution]"
    echo 'where:'
    echo '  resolution = "640x320"'
    echo
    exit 1
fi


filename="$1"
shift
extension=${filename##*.}
fname=${filename%.*}

output="${fname}.mp4"
if [ "$output" = "$filename" ];
then
    output="${fname}a.mp4"
fi


# try using libfdk_aac
audiolib=$(ffmpeg -encoders 2>&1 | grep 'libfdk_aac' | awk '{ print $2; }')
vbr_option='-vbr 2'
audiofreq=''
bitrate=''


# using default AAC
if [ -z "$audiolib" ]
then
    audiolib=$(ffmpeg -encoders 2>&1 | grep 'Advanced Audio Coding' | awk '{ print $2; }')
    vbr_option='-q:a 0.23'
    audiofreq=''
    bitrate=''
fi


# using VORBIS
if [ -z "$audiolib" ]
then
    audiolib=libvorbis
    vbr_option='-aq 4'
    audiofreq='-ar 22050'
    bitrate='-b:a 64k'

fi


until [ -z "$1" ];
do
    size=$(echo $1 | grep -E '[[:digit:]]{3,}x[[:digit:]]{3,}')
    if [ -z "$size" ]
    then
        crf_val=$1
    fi
    shift
done


if [ -z "$crf_val" ]
then
    crf_val=23
fi


echo 'Will be using:' $vidconv
echo 'Audio:' $audiolib
echo 'Threads:' $(nproc)
echo


echo 'cwd:    ' $(pwd)
echo 'input:  ' ${filename}
echo 'src ext:' ${extension}
echo 'output: ' ${output}
echo 'crf:    ' ${crf_val}
if [ -z "$size" ]
then
    echo "using default size"
    echo
    nice -n 19 ionice -c3 $vidconv -i "$filename" -vcodec libx264 -crf ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads $(nproc) "${output}"
else
    echo "use specified size:" $size
    echo
    nice -n 19 ionice -c3 $vidconv -i "$filename" -s "$size" -vcodec libx264 -crf ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads $(nproc) "${output}"
fi



exit_status=$?

if [ $exit_status -eq 0 ]
then
    oldsize=$(wc -c < "${filename}")
    newsize=$(wc -c < "${output}")
    echo old size: $oldsize
    echo new size: $newsize

    if [ "$newsize" -lt "$oldsize" ]
    then
        rm "${filename}"
        newname="${fname}.mp4"
        if [ "${newname}" != "${output}" ]
        then
            mv "${output}" "${fname}.mp4"
        fi
    fi
fi

