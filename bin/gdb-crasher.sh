#!/usr/bin/env sh
#set -o xtrace
gdb -batch -quiet -ex "run" -ex "bt" --args "$@"
