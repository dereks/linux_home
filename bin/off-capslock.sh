#!/usr/bin/env sh

xmodmap -e "remove lock = Caps_Lock"

setxkbmap -option ctrl:nocaps

# to reset options
#setxkbmap -option

