#!/usr/bin/env sh
#set -o xtrace
valgrind -v --read-inline-info=yes --redzone-size=128 --smc-check=all --read-var-info=yes --track-fds=yes --log-file=drd.log --tool=drd --first-race-only=yes --free-is-write=no --show-stack-usage=yes "$@"
