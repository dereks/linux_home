#!/bin/bash
#
# This is used for modifying git diff generated patch to svn compatible patch
#
# Taken from gist: https://gist.github.com/mojodna/44537
#


sed -e "/--- \/dev\/null/{ N; s|^--- /dev/null\n+++ \(.*\)|---\1	(revision 0)\n+++\1	(revision 0)|;}" \
    -e "s/^--- .*/&	(revision $REV)/" \
    -e "s/^+++ .*/&	(working copy)/" \
    -e "s/^diff --git [^[:space:]]*/Index:/" \
    -e "s/^index.*/===================================================================/"
