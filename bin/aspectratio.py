#!/usr/bin/env python2

import sys
import re
from fractions import gcd

def normalize_ratio(s):
    arr = re.split('[x:/]', s)
    if len(arr) != 2:
        return None
    common = gcd(int(arr[0]), int(arr[1]))
    return int(arr[0])/common, int(arr[1])/common

def main():
    if sys.stdin.isatty():
        rawargs = (x.strip() for x in sys.argv[1:])
        args = (x for x in rawargs if x)
    else:
        args = (x.strip() for x in sys.stdin)

    for x in args:
        res = normalize_ratio(x)
        print x,
        if res:
            print '-', '%d:%d' % (res[0], res[1]), '(%f)' % (float(res[0])/res[1])
        else:
            print '- N/A'
    return 0

if __name__ == '__main__':
    exit(main())
