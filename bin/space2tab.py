#!/usr/bin/env python3
import sys

def main(argc, argv):
    for arg in argv[1:]:
        if not arg.startswith('-'):
            rawlines = (x.strip() for x in open(arg))
            for line in rawlines:
                arr = line.split(' ')
                newarr = '\t'.join(arr)
                print(newarr)

    return 0

if __name__ == '__main__':
    exit(main(len(sys.argv), sys.argv))

