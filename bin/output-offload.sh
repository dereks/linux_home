#!/usr/bin/env sh

# sets nouveau to offload intel
xrandr --setprovideroffloadsink nouveau Intel
sleep 1

# reverse PRIME -> render in Intel and pass to nouveau for HDMI output
xrandr --setprovideroutputsource nouveau Intel
sleep 1

# just to trigger dynamic power on
DRI_PRIME=1 glxinfo
sleep 1

xrandr --output HDMI-1-2 --auto --below eDP1
sleep 3

xrandr --output HDMI-1-2 --auto --right-of eDP1

