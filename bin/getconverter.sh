#!/usr/bin/env sh
vidconv=$( (command -v ffmpeg > /dev/null 2>&1 && echo "ffmpeg") || (command -v avconv > /dev/null 2>&1 && echo "avconv") )
printf "%s" $vidconv
