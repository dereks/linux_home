#!/usr/bin/env sh

if [ -t 0 ]; then
	# has std-input, prompt
	echo "This memplot2.sh is only for process which accepts stdin redirection and without prompt."
	exit
else
	# batch mode
	line=$(ls -l /proc/$$/fd/0)
	file="${line##*-> }"

	"$@" < "$file" > /dev/null &
	PID=$!
fi

LOG=/tmp/mem-$PID.log
GNUPLOT_SCRIPT=/tmp/gnuplot-$PID.script

echo "mem log: " $LOG
echo "gnuplot script: " $GNUPLOT_SCRIPT
echo "png output: " mem-graph-$PID.png

cat <<EOF > $GNUPLOT_SCRIPT
set term png small size 800,600
set output "mem-graph-$PID.png"

set ylabel "VSZ"
set y2label "RSS"

set ytics nomirror
set y2tics nomirror in

set yrange [0:*]
set y2range [0:*]

plot "$LOG" using 3 with lines axes x1y1 title "VSZ", \
     "$LOG" using 2 with lines axes x1y2 title "RSS"

EOF

while kill -0 $PID > /dev/null 2>&1 ; do
	ps --pid $PID -o pid=,rss=,vsz= >> $LOG
	gnuplot $GNUPLOT_SCRIPT
	sleep 1
done

