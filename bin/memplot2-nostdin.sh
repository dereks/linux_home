#!/usr/bin/env sh

# batch mode
"$@" > /dev/null &
PID=$!

LOG=/tmp/mem-$PID.log
GNUPLOT_SCRIPT=/tmp/gnuplot-$PID.script

echo "mem log: " $LOG
echo "gnuplot script: " $GNUPLOT_SCRIPT
echo "png output: " mem-graph-$PID.png

cat <<EOF > $GNUPLOT_SCRIPT
set term png small size 1024,768
set output "mem-graph-$PID.png"

set xlabel "Time (s)"
set ylabel "VSZ (kb)"
set y2label "RSS (kb)"

set ytics nomirror
set y2tics nomirror in

set yrange [0:*]
set y2range [0:*]

plot "$LOG" using 2 with lines axes x1y1 title "VSZ (kb)", \
     "$LOG" using 3 with lines axes x1y2 title "RSS (kb)"

EOF

while kill -0 $PID > /dev/null 2>&1 ; do
	#ps --pid $PID -o etime=,vsz=,rss= >> $LOG
	ETIME=`ps --pid $PID -o etime= | sed 's/:\|-/ /g;' | awk '{ print  $4" "$3" "$2" "$1}' | awk '{ print $1+$2*60+$3*3600+$4*86400}' `
	VSZ=`ps --pid $PID -o vsz=`
	RSS=`ps --pid $PID -o rss=`
	echo $ETIME $VSZ $RSS >> $LOG
	gnuplot $GNUPLOT_SCRIPT
	sleep 1
done


