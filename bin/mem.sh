#!/usr/bin/env bash
SCRIPT_DIR=$(dirname "$BASH_SOURCE")
if [ "$1" = "" ];
then
	USER_PIDS=$(ps -u $USER -o pid --no-headers | xargs | sed 's/ /,/g')
	"$SCRIPT_DIR/psmem/ps_mem.py" -p ${USER_PIDS}
else
	"$SCRIPT_DIR/psmem/ps_mem.py" "$@"
fi
