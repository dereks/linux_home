#!/usr/bin/env bash
ctags -R --c++-kinds=+p --fields=+iaS --extra=+q --exclude=*.js --exclude=*.html --exclude=*.lfs --exclude=*.o --exclude=*.so --exclude=*.a --exclude=*.csv --exclude=*.gz --exclude=*.bz2 --exclude=*.zstd --exclude=*.swp --exclude=*.sql --exclude=*.css --exclude=*.txt --exclude=*.ini .

