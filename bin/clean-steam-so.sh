#!/usr/bin/env bash
find ~/.local/share/Steam/  \(  \( -name 'libxcb.so*' -o -name 'libstdc++.so*' -o -name 'libgcc_s.so*' -o -name 'libgpg*.so*' \) -and -not -name '*.py' \)  -print -delete
