#!/usr/bin/env sh
set -x
nice -n 10 7z a -t7z -mm=lzma -mx=9 -mfb=64 -md=32m -ms=on -mmt=6 "$@"
