#!/bin/bash

set -ex

if command -v sqlite3 > /dev/null 2&>1; then
	find ~/.mozilla -name '*.sqlite' -print0 | xargs -0 -I {} -P $(nproc) sqlite3 "{}" "VACUUM;"
	find ~/.config -name '*.sqlite' -print0 | xargs -0 -I {} -P $(nproc) sqlite3 "{}" "VACUUM;"
	find ~/.var -name '*.sqlite' -print0 | xargs -0 -I {} -P $(nproc) sqlite3 "{}" "VACUUM;"
else
	echo Need 'sqlite3' command.
fi

