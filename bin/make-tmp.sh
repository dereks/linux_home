#!/usr/bin/env sh


# for Opera
mkdir -p /tmp/opera/Cache /tmp/opera/Media\ Cache
ln -sf /tmp/opera/Cache ~/.cache/opera/Cache
ln -sf /tmp/opera/Media\ Cache ~/.cache/opera/Media\ Cache


# for Chrome
mkdir -p /tmp/chrome/Cache /tmp/chrome/Media\ Cache
ln -sf /tmp/chrome/Cache ~/.cache/google-chrome/Default/Cache
ln -sf /tmp/chrome/Media\ Cache ~/.cache/google-chrome/Default/Media\ Cache

