#!/usr/bin/env sh
#set -o xtrace
valgrind -v --read-inline-info=yes --redzone-size=128 --smc-check=all --read-var-info=yes --track-fds=yes --log-file=memcheck.log --tool=memcheck --leak-check=full --show-reachable=yes --track-origins=yes  "$@"
