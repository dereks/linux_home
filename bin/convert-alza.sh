#!/usr/bin/env sh

vidconv=$(getconverter.sh)
if [ -z "$vidconv" ]
then
    echo "Unable to find ffmepg or avconv."
    exit 1
fi

if [ -z "$1" ]
then
    echo "usage: convert.sh inputfile [resolution]"
    echo 'where:'
    echo '  resolution = "640x320"'
    echo
    exit 1
fi


filename="$1"
shift
extension=${filename##*.}
fname=${filename%.*}

output="${fname}.avi"
if [ "$output" = "$filename" ];
then
    output="${fname}a.avi"
fi


audiolib=libmp3lame
vbr_option='-aq 4'
audiofreq='-ar 44100'
bitrate='-b:a 96k'


until [ -z "$1" ];
do
    size=$(echo $1 | grep -E '[[:digit:]]{3,}x[[:digit:]]{3,}')
    if [ -z "$size" ]
    then
        crf_val=$1
    fi
    shift
done


if [ -z "$crf_val" ]
then
    crf_val=9
fi


echo 'Will be using:' $vidconv
echo 'Audio:' $audiolib
echo 'Threads:' $(nproc)
echo


echo 'cwd:    ' $(pwd)
echo 'input:  ' ${filename}
echo 'src ext:' ${extension}
echo 'output: ' ${output}
echo 'crf:    ' ${crf_val}
if [ -z "$size" ]
then
    echo "using default size"
    echo
    nice -n 15 ionice -c3 $vidconv -i "$filename" -vcodec libxvid -q:v ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads $(nproc) "${output}"
else
    echo "use specified size:" $size
    echo
    nice -n 15 ionice -c3 $vidconv -i "$filename" -s "$size" -vcodec libxvid -q:v ${crf_val} -acodec ${audiolib} ${audiofreq} ${vbr_option} ${bitrate} -threads $(nproc) "${output}"
fi



exit_status=$?

if [ $exit_status -eq 0 ]
then
    oldsize=$(wc -c < "${filename}")
    newsize=$(wc -c < "${output}")
    echo old size: $oldsize
    echo new size: $newsize

    if [ "$newsize" -lt "$oldsize" ]
    then
        rm "${filename}"
        newname="${fname}.avi"
        if [ "${newname}" != "${output}" ]
        then
            mv "${output}" "${fname}.avi"
        fi
    fi
fi

