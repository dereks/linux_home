#!/usr/bin/env bash

while read -r line; do
	if [[ ! -f "$line" ]]; then
		echo $line
	fi
done < "$1"
