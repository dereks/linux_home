#!/usr/bin/env python
import hashlib
import os
import pprint
import sys

MIN_SIZE = 50 * 1024
MAX_BUF = 65536

def main(argc, argv):
    size_dct = get_same_size(argv[1:])
    print len(size_dct), 'potential duplicate groups found.'

    dup_file = {}
    #old_txt = ''
    sizes = size_dct.keys()
    sizes.sort()
    for size in sizes:
        files = size_dct[size]
        #print '\b' * (len(old_txt)+1),
        #print ' ' * (len(old_txt)+1),
        #print '\b' * (len(old_txt)+1),
        #old_txt = '\bChecking category: size=%d count=%d' % (size, len(files))
        #print old_txt,
        dup_file.update(display_dup(get_duplicate(files)))
    print

    if dup_file:
        #for k, v in dup_file.iteritems():
        #    v.sort()
        #    print k[:7], v
        return 1
    else:
        print 'No duplicates found.'

    return 0


def display_dup(dct):
    if dct:
        pprint.pprint(dct)
        print
    return dct


def get_duplicate(files):
    d = {}
    for f in files:
        the_hash = get_hash(f)
        d[the_hash] = d.get(the_hash, []) + [f]
    newd = {}
    for k, v in d.iteritems():
        if len(v) > 1:
            newd[k] = v
    return newd


def get_same_size(paths):
    d = {}
    for p in paths:
        for f in get_files_recursive(p):
            st = os.stat(f)
            if st.st_size > MIN_SIZE:
                d[st.st_size] = d.get(st.st_size, []) + [f]
    newd = {}
    for k, v in d.iteritems():
        if len(v) > 1:
            newd[k] = v
    return newd


def get_files_recursive(thepath):
    """ returns generator on all the files in thepath recursively """
    for dirpath, dirnames, filenames in os.walk(thepath):
        for f in filenames:
            yield os.path.join(dirpath, f)


def get_hash(fname):
    sha1 = hashlib.sha1()
    with open(fname, 'rb') as f:
        while True:
            buf = f.read(MAX_BUF)
            if len(buf) == 0:
                break
            elif len(buf) < MAX_BUF:
                sha1.update(buf)
                break
            else:
                sha1.update(buf)
    return sha1.hexdigest()[:10]


if __name__ == '__main__':
    exit(main(len(sys.argv), sys.argv))
