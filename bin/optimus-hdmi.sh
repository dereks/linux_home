#!/usr/bin/env sh

cat /proc/acpi/bbswitch
sleep 1
primusrun glxinfo
sleep 1
cat /proc/acpi/bbswitch
primusrun intel-virtual-output
sleep 1
xrandr --output VIRTUAL1 --auto --right-of eDP1
