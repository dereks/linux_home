#!/usr/bin/env sh

PIDS=$(ps -ef | grep virtual | grep -v grep | awk '{print $2}' | xargs)

#sudo renice -n 18 -p $PIDS
#sudo ionice -c 2 -n 7 -p $PIDS

sudo renice -n 19 -p $PIDS
sudo ionice -c 3 -p $PIDS
