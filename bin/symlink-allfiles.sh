#!/usr/bin/env bash
find "$1" -type f -exec bash -c 'test -L $(basename "$1") && ln -fs "$1" $(basename "$1") || ln -s "$1" $(basename "$1")' foo {}  \;
