# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

PATH=$HOME/bin:$HOME/opt/bin:$HOME/.local/bin:$PATH

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi


[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# sometimes the bash-completion pkg couldn't load git ps1
if [ -f /usr/share/git/completion/git-prompt.sh ]; then
	. /usr/share/git/completion/git-prompt.sh

elif [ -f /usr/share/git-core/contrib/completion/git-prompt.sh ]; then
	. /usr/share/git-core/contrib/completion/git-prompt.sh

elif [ -f ~/.git-prompt.sh ]; then
	. ~/.git-prompt.sh

else
	__git_ps1()
	{
		local gitbranch=`git branch 2> /dev/null`
		printf -- "(git:%s)" "$gitbranch"
	}

fi



__mygit_projectname ()
{
	# svn is here
	if [[ -d ".svn" ]]
	then
		local current_branch=`svn info 2> /dev/null | grep 'Relative URL:' | cut -d ' ' -f 3- | cut -b 3- `
		if [ ! -z "$current_branch" ]; then
			printf " svn:(%s)" "$current_branch"
			return
		fi
	fi

	local origin=`git config --get remote.origin.url`
	if [ -z "$origin" ]; then
		return
	fi

	local base_origin=`basename "$origin"`
	local projname="${base_origin%.*}"
	local branchname=`git branch | grep '*' | cut -d ' ' -f 2 `
	local githash=`git log -n1 --format=%h`
	if [ -z "$projname" ]; then
		return
	fi
	#printf " git:%s%s" "$projname" "$(__git_ps1)"
	printf " git:%s%s" "$projname" "($githash  $branchname) "
}


if [ "$color_prompt" = yes ]; then
	PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \w\[\033[01;32m\]]\[\033[00m\]$(__mygit_projectname)\[\033[01;32m\]\$\[\033[00m\] '
    #PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\[\033[00m\]$(__git_ps1)\[\033[01;32m\]\$\[\033[00m\] '
    # PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1)\$ '
else
	PS1='\[\033[01;31m\][\h\[\033[01;36m\] \w\[\033[01;31m\]]\[\033[00m\]$(__mygit_projectname)\[\033[01;32m\]\$\[\033[00m\] '
    #PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\[\033[00m\]$(__git_ps1)\[\033[01;32m\]\$\[\033[00m\] '
    # PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w$(__git_ps1)\$ '
fi
unset color_prompt force_color_prompt

# debugging shell scripts with "set -x" in shell script
export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

fi

export EDITOR=vim
export VISUAL=vim
export GIT_PS1_SHOWDIRTYSTATE=auto
export GIT_PS1_SHOWSTASHSTATE=auto
export GIT_PS1_SHOWUPSTREAM=auto
export GIT_PS1_SHOWUNTRACKEDFILES=auto
export LESS=" -RFX "

# some more ls aliases
alias cp='cp --reflink=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias cp='cp --reflink=auto '

GREP_OPTION="--exclude-dir=.svn --exclude-dir=.git --exclude-from=$HOME/.grep_exclude "
alias grep="grep --color=auto $GREP_OPTION "
alias fgrep="fgrep --color=auto $GREP_OPTION "
alias egrep="egrep --color=auto $GREP_OPTION "

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

uname -a | grep -iE 'Microsoft|Cygwin' > /dev/null
HAS_MS=$?
if [[ $HAS_MS -eq 0 ]]; then

	uname -a | grep 'WSL2' > /dev/null
	USING_WSL2=$?

	if [[ $USING_WSL2 -eq 0 ]]; then
		echo "Microsoft WSL2 found... "
		export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
		export LIBGL_ALWAYS_INDIRECT=1
	else
		echo "Microsoft found... Set DISPLAY to 127.0.0.1"
		export DISPLAY=127.0.0.1:0
	fi
fi


test -s ~/.alias && . ~/.alias || true
[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"


# gvim often complains about AT-SPI wand atk-bridge warnings
# this export will disable the WARNING
# AT-SPI or atk-bridge
# ATSPI = Assistive Technology Service Provider Interface
export NO_AT_BRIDGE=1

# ask cygwin to use native symlink that available in NTFS
export CYGWIN=winsymlinks:nativestrict

# source aws credential
[ -f "$HOME/aws-cred.bz2" ] && . <(bzip2 -dkc aws-cred.bz2)

ulimit -Sn 32768
echo 'ulimit -Sn = ' `ulimit -Sn`

