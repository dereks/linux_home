python
import sys
import os
sys.path.insert(0, os.path.join(os.getenv('HOME'), 'bin/python-dbg-stl-print'))
from libstdcxx.v6.printers import register_libstdcxx_printers
register_libstdcxx_printers(None)
end

set print pretty on
set print object on
set print vtbl on

set history save on
set history filename ~/.gdb_history

set tui border-kind acs
set tui border-mode normal
set tui active-border-mode bold

